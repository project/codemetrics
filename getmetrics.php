<?php
/** 
 * @file
 * Command line tool for generating code metrics reports.
 *
 * Usage:
 *    % php ./getmetrics.php [/path/to/code/base]
 *
 * If no path is given, the current directory is used.
 *
 */
include_once('codemetrics.module');

$ignored_file_extns = array('conf', 'gif', 'htm', 'html', 'ico', 
			    'info', 'ini', 'jpg',  'png', 'po',  
			    'pot', 'psd', 'swf', 'txt');

$ignored_dirs = array('po', 'patches', 'icons', 'CVS', '.svn', '_darcs');

/** 
 *
 */
function get_option(&$argv, $option, $default = FALSE) {
  if (($index = array_search($option, $argv)) !== FALSE) {
    unset($argv[$index]);
    return TRUE;
  }
  return $default;
}

/** Skip emacs backup files, documentation files (e.g. README, TODO,
   CREDITS), and any non-code resources
*/
function is_boring_file($path) {
  global $ignored_file_extns;
  $file_parts = pathinfo($path);
  $ext = isset($file_parts['extension']) ? strtolower($file_parts['extension']): '';
  if (
    ereg('~$', $path) || 
    strtoupper($path) === $path ||
    in_array($ext, $ignored_file_extns)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/** Skip CVS/SVN/Darcs directories */
function is_boring_dir($path) {
  global $ignored_dirs;
  $excluded = implode('|', $ignored_dirs);
  $excluded = '('. str_replace(".", "\.", $excluded) . ')$';
  return ereg($excluded, $path);
  //return ereg('/(po|patches|icons|CVS|\.svn|_darcs)$', $path);
}

/** 
 * Iterate over files in given directory, gathering statistics 
 */
function analyze_dir($dir, &$file_counts) {
  foreach (glob("$dir/*") as $file) {
    if (is_link($file)) {
      continue; // Skip symbolic links
    }
    elseif (is_dir($file)) {
      if (!is_boring_dir($file)) {
	analyze_dir($file, $file_counts);
      }
    }
    elseif (!is_boring_file($file) && is_readable($file)) {
      $type = codemetrics_file_type($file, 'drupal6');

      codemetrics_analyze($file, 'drupal6', $type, $file_counts);
      if (CODEMETRICS_VERBOSE) {
        printf("%s: %d bytes", 
	       $file, 
	       $file_counts[$file]['filesize']);
	if ($type != 'unknown') {
	  printf(", %d lines of code", $file_counts[$file]['stats']['lines']);
	}
	print "\n";
      }
    }
    else {
      // not readable or ignored file type
      if (CODEMETRICS_VERBOSE) {
	echo "$file: ignored\n";
      }
    }
  }
}

function report_json($file_counts) {
  $output = <<<END_OF_HEADER
{
 types: {
  "sourcefile" : {
    pluralLabel: "sourcefiles"
    }
  },
  "items" : [
END_OF_HEADER;
  foreach ($file_counts as $file => $item) {
    $output .= codemetrics_output_json($file, $item);
  }
  $output = rtrim($output, ",\n");
  $output .= "\n";
  $output .= <<<END_OF_FOOTER
	     ],
    "properties" : {
      "lines" : {
        "valueType" : "number"
	  },
    }
}
END_OF_FOOTER;
 echo $output;
}


function summary($file_counts) {
  $lines = $num_files = $num_bytes = $num_funcs = 0;
  foreach ($file_counts as $stat) {
    $num_files++;
    $num_bytes += $stat['filesize'];
    if (isset( $stat['tokencount']['function'])) {
      $num_funcs += $stat['tokencount']['function'];
    }
    if (isset( $stat['stats']['lines'])) {
      $lines += $stat['stats']['lines'];
    }
  }
  printf("%d files (%s KB), containing %d lines of code and %d function definitions.\n",
	 $num_files, 
	 number_format($num_bytes / 1024.0, 0), 
	 $lines, 
	 $num_funcs);
}

/** */
function main($argv) {
  codemetrics_init();
  if (get_option($argv, '-?') || 
      get_option($argv, '-h') ||
      get_option($argv, '--help')) {
    die("Usage: " . $argv[0] . " [PATH] [-a] [-w] [-v] [-j]\n");
  }
  define('CODEMETRICS_EVERYTHING', get_option($argv, '-a'));
  define('CODEMETRICS_WHITESPACE', get_option($argv, '-w'));
  define('CODEMETRICS_VERBOSE', get_option($argv, '-v'));
  define('CODEMETRICS_JSON', get_option($argv, '-j'));
  $paths = empty($argv) ? array(dirname(__FILE__)) : $argv;
  foreach ($paths as $path) {
    $path = realpath($path);
    //print("Analyzing code directory $path...\n");
    analyze_dir($path, $file_counts);
    if (CODEMETRICS_JSON) {
      report_json($file_counts);
    }
    else {
      echo codemetrics_report($file_counts, 'drupal6', 'text');
      summary($file_counts);
    }
  }
}

main(array_slice($argv, 1));
