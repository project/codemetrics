<?php
/**
 * @file
 * Common functions for reporting / analysis for generic files.
 * 
 */

include_once('generic_common.inc');

/** Generic file analyzer for 'c' style code */
function codemetrics_generic_analyze($file, &$counts) {
  $stats = array();
  $stats['lines'] = 0;
  // Staggeringly inefficient, but who cares
  $lines = explode("\n", file_get_contents($file));
  $in_comment = FALSE;
  foreach ($lines as $line) {
    $line = trim($line);
    if (!CODEMETRICS_EVERYTHING) {
      if (!CODEMETRICS_WHITESPACE && strlen($line) == 0)
	continue;
      if (substr($line, 0, 2) == '//')
	continue;
      if (strpos($line, '/*') !== FALSE)
	$in_comment = TRUE;
      if (strpos($line, '*/') !== FALSE)
	$in_comment = FALSE;
    }
    _codemetrics_inc_array($stats, 'lines');
    if ($in_comment) {
      _codemetrics_inc_array($stats, 'comment-lines');
    }
  }
  $counts['stats'] = $stats;
  $counts['cocomo'] = codemetrics_cocomo($counts['stats']['lines']/1000, 5000, 'organic');   
  return $counts;
}

