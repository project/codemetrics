<?php
/**
 * @file
 * Provides reporting for CSS files.
 */

function codemetrics_css_report_stats($file, $stats) {
  $output = codemetrics_do_report($stats, "arsort", "Statistics", 
				  array("Statistic", "Value"));
  return $output;
}

function codemetrics_css_report($file, $count) {
  $output = codemetrics_css_report_stats($file, $count['stats']);
  return $output;
}



