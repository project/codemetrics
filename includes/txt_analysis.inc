<?php
/**
 * @file
 * Common functions for reporting / analysis for generic text files.
 * 
 */

include_once('txt_common.inc');

/** Txt file analyzer for 'c' style code */
function codemetrics_txt_analyze($file, &$counts) {
  $string = file_get_contents($file);
  return codemetrics_txt_analyze_string($string, $counts);
}

/** Txt analyzer for 'c' style code */
function codemetrics_txt_analyze_string($string, &$counts) {
  // Staggeringly inefficient, but who cares
  $lines = explode("\n", $string);
  $in_comment = FALSE;
  $stats = array();
  $stats['lines'] = 0;
  foreach ($lines as $line) {
    $line = trim($line);
    if (!CODEMETRICS_EVERYTHING) {
      if (!CODEMETRICS_WHITESPACE && strlen($line) == 0)
	continue;
      if (substr($line, 0, 2) == '//')
	continue;
      if (strpos($line, '/*') !== FALSE)
	$in_comment = TRUE;
      if (strpos($line, '*/') !== FALSE)
	$in_comment = FALSE;
    }
    _codemetrics_inc_array($stats, 'lines');
    if ($in_comment) {
      _codemetrics_inc_array($stats, 'comment-lines');
    }
  }
  $counts['stats'] = $stats;
  $counts['cocomo'] = codemetrics_cocomo($counts['stats']['lines']/1000, 5000, 'organic');   
  return $counts;
}

