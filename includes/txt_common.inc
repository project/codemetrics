<?php
/**
 * @file
 * Common functions for generic analysis / reporting
 *
 */


/* Map file extensions to types. */
function codemetrics_txt_file_extensions() {
  return array(
    'txt' => array(
      'css' =>'css',
      'js' =>'javascript',
      'javascript' =>'javascript',
      'txt' =>'txt',
      'text' =>'txt',
      'sh' =>'txt',
      'pl' =>'txt',
      'htm' =>'html',
      'html' =>'html',
      '' => 'txt',
      ),
    );	       
}
