<?php
/**
 * @file
 * Common functions for CSS analysis and reporting
 */


/** 
 * Returns an array of file extensions this plugin handles, mapping
 * to the canonical name of the type. 
 */
function codemetrics_css_file_extensions() {
  return array(
    'generic' => array('css' =>'css')
    );
}
