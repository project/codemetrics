<?php
/**
 * @file
 * Provides analysis for SQL files.
 */

include_once('sql_common.inc');

/** Sql file analyzer */
function codemetrics_sql_analyze($file, &$counts) {
  $string = file_get_contents($file);
  return codemetrics_sql_analyze_string($string, $counts);
}

/** Sql file analyzer */
function codemetrics_sql_analyze_string($string, &$counts) {
  $lines = explode("\n", $string);
  $stats = array();
  $stats['lines'] = 0;
  foreach ($lines as $line) {
    $line = trim($line);
    if (!CODEMETRICS_EVERYTHING) {
      if (!CODEMETRICS_WHITESPACE && strlen($line) == 0)
	continue;
      if (substr($line, 0, 1) == '#')
	_inc_array($stats, 'comment-lines');
      continue;
      if (substr($line, 0, 2) == '--')
	_codemetrics_inc_array($stats, 'comment-lines');
      continue;
    }
    _codemetrics_inc_array($stats, 'lines');
  }
  $counts['stats'] = $stats;
  return $counts;
}

