<?php
/**
 * @file
 * Common functions for SQL analysis and reporting
 */

/** 
 * Returns an array of file extensions this plugin handles, mapping
 * to the canonical name of the type. 
 */
function codemetrics_sql_file_extensions() {
  return array(
    'generic' => array(
      'mysql' =>'sql',
      'mysql40' =>'sql',
      'mysql41' =>'sql',
      'sql' =>'sql',
      'pgsql' =>'sql',
      ),
    );	   
}
