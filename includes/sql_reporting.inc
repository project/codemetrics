<?php
/**
 * @file
 * Provides reporting for SQL files.
 */

function codemetrics_sql_report_stats($file, $stats) {
  return codemetrics_do_report($stats, "arsort", "Statistics", 
			array("Statistic", "Value"));
}

function codemetrics_sql_report($file, $count) {
  codemetrics_sql_report_stats($file, $count['stats']);
}



