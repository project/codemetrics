<?php
/**
 * @file
 * Provides reporting for DRUPAL6 files.
 */

include_once('drupal6_common.inc');

// statistics
function codemetrics_drupal6_report_tokencount($file, $tokencount, $mode) {
  return codemetrics_php_report_tokencount($file, $tokencount, $mode);
}

// statistics
function codemetrics_drupal6_report_stats($file, $stats, $mode) {
  if (!count($stats)) return;

  $statvals = array();
  foreach ($stats as $key => $val) {
    if (CODEMETRICS_VERBOSE) {
      $statvals[$key] = empty($stats[$key]) ? '0' : $stats[$key];
    }
    elseif (!empty($stats[$key])) {
      $statvals[$key] = $stats[$key];
    }
  }
  $output = codemetrics_do_report($statvals, "ksort", "Statistics", 
				   array("Statistic", "Value"), $mode);

  return $output;
}

// variable usage
function codemetrics_drupal6_report_vars($fileref, $vars, $mode) {
  if (!count($vars)) return;
  $newvars = array();
  foreach ($vars as $variable => $list) {
    $uniq = array_unique($list);
    if ($mode == 'html') {
      foreach ($uniq as &$item) {
	$item = "<a href='/node/$fileref#".$item."'>$item</a>";
      }
    }
    $newvars[$variable] = implode(", ", $uniq);
  }
  $output = codemetrics_do_report($newvars, "ksort", "Variables", 
				   array("Variable", "Line"), $mode);

  return $output;
}

// function definition usage
function codemetrics_drupal6_report_funcdefs($fileref, $funcs, $mode) {
  if (!count($funcs)) return;
  $funcdef = array();
  foreach ($funcs as $func => $line) {
    if ($mode == 'html') {
      $item = "<a href='/node/$fileref#".$line."'>$line</a>";
      $funcdef[$func] = $item;
    }
    else {
      $funcdef[$func] = $line;
    }
  }
  $output = codemetrics_do_report($funcdef, "ksort", "Function definitions", 
				   array("Function defn", "Line"), $mode);
  return $output;
}

// function calls 
function codemetrics_drupal6_report_funccalls($fileref, $funcs, $mode) {
  if (!count($funcs)) return;
  $output = '';
  if (isset($funcs['drupal6_internal']) && count($funcs['drupal6_internal'])) {
    $drupal_func_calls = $funcs['drupal6_internal'];
    $dfc = array();
    foreach ($drupal_func_calls as $fn => $list) {
      $uniq = array_unique($list);
      if ($mode == 'html') {
	foreach ($uniq as &$item) {
	  $item = "<a href='/node/$fileref#".$item."'>$item</a>";
	}
      }
      $dfc[$fn] = implode(", ", $uniq);
    }
    $output = codemetrics_do_report($dfc, "ksort", "Drupal func calls",
				    array("Built-in func calls", "Line"), $mode);
  }
  if (isset($funcs['internal']) && count($funcs['internal'])) {
    $int_func_calls = $funcs['internal'];
    $ifc = array();
    foreach ($int_func_calls as $fn => $list) {
      $uniq = array_unique($list);
      if ($mode == 'html') {
	foreach ($uniq as &$item) {
	  $item = "<a href='/node/$fileref#".$item."'>$item</a>";
	}
      }
      $ifc[$fn] = implode(", ", $uniq);
    }
    $output .= codemetrics_do_report($ifc, "ksort", "Internal func calls", 
				     array("Built-in func calls", "Line"), $mode);
  }

  if (isset($funcs['user']) && count($funcs['user'])) {
    $user_func_calls = $funcs['user'];
    $ufc = array();
    foreach ($user_func_calls as $fn => $list) {
      $uniq = array_unique($list);
      if ($mode == 'html') {
	foreach ($uniq as &$item) {
	  $item = "<a href='/node/$fileref#".$item."'>$item</a>";
	}
      }
      $ufc[$fn] = implode(", ", $uniq);
    }
    $output .= codemetrics_do_report($ufc, "ksort", "User func calls", 
				     array("Function", "Line"), $mode);
  }
  return $output;
}

// constants usage
function codemetrics_drupal6_report_consts($file, $consts, $mode) {
  return codemetrics_php_report_consts($file, $consts, $mode);
}

// classes usage
function codemetrics_drupal6_report_classes($file, $classes, $mode) {
  return codemetrics_php_report_classes($file, $classes, $mode);
}

// meta values
function codemetrics_drupal6_report_meta($file, $meta, $mode) {
  return codemetrics_php_report_meta($file, $meta, $mode);
}

/**
 *
 */
function codemetrics_drupal6_report_tokens($file, $tokens, $mode) {
  return codemetrics_php_report_tokens($file, $tokens, $mode);
}

/**  
 Report based on the information supplied in $count.
   @param $count - array containing various kinds of statistics.
*/
function codemetrics_drupal6_report($file, $count, $mode) {
  $output = codemetrics_drupal6_report_stats($file, $count['stats'], $mode);
  $output .= codemetrics_drupal6_report_vars($file, $count['vars'], $mode);
  $output .= codemetrics_drupal6_report_funcdefs($file, $count['funcdefs'], $mode);
  $output .= codemetrics_drupal6_report_funccalls($file, $count['funccalls'], $mode);
  $output .= codemetrics_drupal6_report_tokencount($file, $count['tokencount'], $mode);
  $output .= codemetrics_drupal6_report_consts($file, $count['consts'], $mode);
  $output .= codemetrics_drupal6_report_classes($file, $count['classes'], $mode);
  if (CODEMETRICS_VERBOSE) {
    $output .= codemetrics_drupal6_report_tokens($file, $count['tokens'], $mode);
  }
  return $output;
}

