<?php
/**
 * @file
 * Provides reporting for generic file types.
 */

function codemetrics_generic_report_stats($stats) {
  $output = codemetrics_do_report($stats, "arsort", "Statistics", 
				  array("Statistic", "Value"));
  return $output;
}

function codemetrics_generic_report($count) {
  $output = codemetrics_generic_report_stats($count['stats']);
  return $output;
}

