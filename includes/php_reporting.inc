<?php
/**
 * @file
 * Provides reporting for PHP files.
 */

include_once('php_common.inc');

// statistics
function codemetrics_php_report_tokencount($file, $tokencount, $mode) {
  if (!count($tokencount)) return;

  $tokvals = array();
  foreach ($tokencount as $key => $val) {
    if (CODEMETRICS_VERBOSE) {
      $tokvals[$key] = empty($tokencount[$key]) ? '0' : $tokencount[$key];
    }
    elseif (!empty($tokencount[$key])) {
      $tokvals[$key] = $tokencount[$key];
    }
  }
  $output = codemetrics_do_report($tokvals, "ksort", "Token count", 
				   array("Token", "Count"), $mode);
  return $output;
}

// statistics
function codemetrics_php_report_stats($file, $stats, $mode) {
  if (!count($stats)) return;

  $statvals = array();
  foreach ($stats as $key => $val) {
    if (CODEMETRICS_VERBOSE) {
      $statvals[$key] = empty($stats[$key]) ? '0' : $stats[$key];
    }
    elseif (!empty($stats[$key])) {
      $statvals[$key] = $stats[$key];
    }
  }
  $output = codemetrics_do_report($statvals, "ksort", "Statistics", 
				   array("Statistic", "Value"), $mode);

  return $output;
}

// variable usage
function codemetrics_php_report_vars($fileref, $vars, $mode) {
  if (!count($vars)) return;
  $newvars = array();
  foreach ($vars as $variable => $list) {
    $uniq = array_unique($list);
    if ($mode == 'html') {
      foreach ($uniq as &$item) {
	$item = "<a href='/node/$fileref#".$item."'>$item</a>";
      }
    }
    $newvars[$variable] = implode(", ", $uniq);
  }
  $output = codemetrics_do_report($newvars, "ksort", "Variables", 
				   array("Variable", "Line"), $mode);

  return $output;
}

// function definition usage
function codemetrics_php_report_funcdefs($fileref, $funcs, $mode) {
  if (!count($funcs)) return;
  $funcdef = array();
  foreach ($funcs as $func => $line) {
    if ($mode == 'html') {
      $item = "<a href='/node/$fileref#".$line."'>$line</a>";
      $funcdef[$func] = $item;
    }
    else {
      $funcdef[$func] = $line;
    }
  }
  $output = codemetrics_do_report($funcdef, "ksort", "Function definitions", 
				   array("Function defn", "Line"), $mode);
  return $output;
}

// function calls 
function codemetrics_php_report_funccalls($fileref, $funcs, $mode) {
  if (!count($funcs)) return;
  if (isset($funcs['internal']) && count($funcs['internal'])) {
    $int_func_calls = $funcs['internal'];
    $ifc = array();
    foreach ($int_func_calls as $fn => $list) {
      $uniq = array_unique($list);
      if ($mode == 'html') {
	foreach ($uniq as &$item) {
	  $item = "<a href='/node/$fileref#".$item."'>$item</a>";
	}
      }
      $ifc[$fn] = implode(", ", $uniq);
    }
    $output = codemetrics_do_report($ifc, "ksort", "Internal func calls", 
				     array("Built-in func calls", "Line"), $mode);
  }

  if (isset($funcs['user']) && count($funcs['user'])) {
    $user_func_calls = $funcs['user'];
    $ufc = array();
    foreach ($user_func_calls as $fn => $list) {
      $uniq = array_unique($list);
      $ufc[$fn] = implode(", ", $uniq);
    }
    $output = codemetrics_do_report($ufc, "ksort", "User func calls", 
				     array("Function", "Line"), $mode);
  }
  return $output;
}

// constants usage
function codemetrics_php_report_consts($file, $consts, $mode) {
  if (!count($consts)) return;
  $output = codemetrics_do_report($consts, "ksort", "Constants", 
				   array("Constant defn", "Line"), $mode);
  return $output;
}

// classes usage
function codemetrics_php_report_classes($file, $classes, $mode) {
  if (!count($classes)) return;
  $output = codemetrics_do_report($classes, "ksort", "Classes", 
				   array("Class defn", "Line"), $mode);
  return $output;
}

// meta values
function codemetrics_php_report_meta($file, $meta, $mode) {
  if (!count($meta)) return;
  $output = codemetrics_do_report($meta, "arsort", "Meta", 
				   array("Meta", "Value"), $mode);
  return $output;
}

/**
 *
 */
function codemetrics_php_report_tokens($file, $tokens, $mode) {
  global $tokenmap;
  if (!count($tokens)) return;

  // raw token count
  $output = codemetrics_do_report($tokens, "arsort", "Tokens", 
				   array("Token", "Count"), $mode);

  // detailed stats by token type
  $tok_count = array();
  foreach ($tokenmap as $tokenname => $token) {
    foreach (array_values($token) as $stat_key) {
      if (CODEMETRICS_VERBOSE) {
	$tok_count[$stat_key] = empty($tokens[$tokenname]) ? '0' : $tokens[$tokenname];
      }
      elseif (!empty($tokens[$tokenname])) {
	$tok_count[$stat_key] = $tokens[$tokenname];
      }
    }
  }

  $output = codemetrics_do_report($tok_count, "arsort", "Statistics", 
				   array("Statistic", "Value"), $mode);
  return $output;
}

/**  
 Report based on the information supplied in $count.
   @param $count - array containing various kinds of statistics.
*/
function codemetrics_php_report($file, $count, $mode) {
  $output = codemetrics_php_report_stats($file, $count['stats'], $mode);
  $output .= codemetrics_php_report_vars($file, $count['vars'], $mode);
  $output .= codemetrics_php_report_funcdefs($file, $count['funcdefs'], $mode);
  $output .= codemetrics_php_report_funccalls($file, $count['funccalls'], $mode);
  $output .= codemetrics_php_report_tokencount($file, $count['tokencount'], $mode);
  $output .= codemetrics_php_report_consts($file, $count['consts'], $mode);
  $output .= codemetrics_php_report_classes($file, $count['classes'], $mode);
  if (CODEMETRICS_VERBOSE) {
    $output .= codemetrics_php_report_tokens($file, $count['tokens'], $mode);
  }
  return $output;
}

