<?php
/**
 * @file
 * Provides reporting for generic text file types.
 */

function codemetrics_txt_report_stats($file, $stats) {
  $output = codemetrics_do_report($stats, "arsort", "Statistics", 
				  array("Statistic", "Value"));
  return $output;
}

function codemetrics_txt_report($file, $count) {
  $output = codemetrics_txt_report_stats($file, $count['stats']);
  return $output;
}

