<?php
/**
 * @file
 * Common code for Drupal 6 reporting and analysis.
 */

include_once(PATH_TO_GESHI."/geshi/geshi.php");
include_once(PATH_TO_GESHI."/geshi-extra/drupal6.php");

#print_r($language_data);
//print_r($language_data['KEYWORDS'][5]);

global $drupal6_internal_funcs;
global $drupal6_internal_consts;

$drupal6_internal_funcs = $language_data['KEYWORDS'][5];
$drupal6_internal_consts = $language_data['KEYWORDS'][6];

/* Map file extensions to types. */
function codemetrics_drupal6_file_extensions() {
  global $drupal6_internal_funcs;
  global $drupal6_internal_consts;
  global $language_data;

  return array(
    'drupal6' => array(
      'class' =>'drupal6',
      'engine' =>'drupal6',
      'inc' =>'drupal6',
      'install' =>'drupal6',
      'module' =>'drupal6',
      'php' =>'drupal6',
      'profile' =>'drupal6',
      'theme' =>'drupal6',
      'test' =>'drupal6',
    ),
  );	       
}
