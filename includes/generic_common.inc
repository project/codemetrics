<?php
/**
 * @file
 * Common functions for generic analysis / reporting
 *
 */

function codemetrics_generic_supports_types() {
  return array('javascript' => 'generic', 
	       'css' => 'generic',
	       'txt' => 'generic',
	       'sh' => 'generic',
	       'pl' => 'generic',
    );
}

/* Map file extensions to types. */
function codemetrics_generic_file_extensions() {
  return array(
	       'css' =>'css',
	       'js' =>'javascript',
	       'javascript' =>'javascript',
	       'txt' =>'txt',
	       'text' =>'txt',
	       'sh' =>'txt',
	       'pl' =>'txt',
	       'htm' =>'html',
	       'html' =>'html',
    );	       
}
