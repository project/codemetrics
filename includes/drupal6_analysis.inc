<?php
/**
 * @file
 * Provides analysis for DRUPAL6 files.
 */
include_once('drupal6_common.inc');

/** */
function codemetrics_drupal6_token_process($token, &$tokencount, &$stats) {
  global $tokenmap;
  if (count($token)) {
    $tokenname = token_name($token[0]);
  }
  else {
    _codemetrics_error('codemetrics', "$token is a simple token...");
    $tokenname = $token;
  }
  if (in_array($tokenname, array_keys($tokenmap))) {
    foreach($tokenmap[$tokenname] as $stat) {
      _codemetrics_inc_array($tokencount, $stat);
    }
  }
  else {
    _codemetrics_error("UNKNOWN <$tokenname>\n");
  }
  codemetrics_drupal6_token_postprocess($token, $stats);
}

/** Count lines, optionally echo token  */
function codemetrics_drupal6_token_postprocess($token, &$stats) {
  global $lineno;
  if (is_string($token)) {
    $text = $token;
    $id =  0;
  }
  else {
    $text = $token[1];
    $token_name = token_name($token[0]);
  }
  if (
    $text == "\n"  || 
    $text == "\r" || 
    $text == "\r\n" ||
    !(strpos($text, "\n") === FALSE) ||
    !(strpos($text, "\r")  === FALSE) ||
    !(strpos($text, "\r\n") === FALSE) 
    ) {
    $linecount = _codemetrics_linecount($text); 
    $stats['lines'] += $linecount;
    $lineno += $linecount;
  }
  else {
  }
  _codemetrics_showtext($text);
}

/** Returns the next non-whitespace token, as a string. */
function codemetrics_drupal6_token_peek(&$tokens) {
  $rewind = 0;
  $token = next($tokens);
  $rewind++;
  while (count($token) && ($token[0] == T_WHITESPACE)) {
    $token = next($tokens);
    $rewind++;
  }
  while ($rewind--) {prev($tokens);}
  return $token;
}

/** Skips to the next non-whitespace token. */
function codemetrics_drupal6_token_skip(&$tokens) {
  $token = next($tokens);

  while (count($token) && 
	 is_numeric($token[0]) && 
	 token_name($token[0]) == "T_WHITESPACE") {
    $text = $token[1];
    codemetrics_drupal6_token_postprocess($token, $stats);
    $token = next($tokens);	  
  }
  return $token;
}

/** Wrapper for codemetrics_drupal6_analyze_string() provides analysis
 * for DRUPAL6 code files. See codemetrics_drupal6_analyze_string().
 */
function codemetrics_drupal6_analyze($file, &$counts) {
  $string = file_get_contents($file);
  return codemetrics_drupal6_analyze_string($string, $counts);
}

/** Analyzer for DRUPAL6 code. Returns an array of all file and token
 * statistics, as well as variable and function usage, for the given
 * string.
 */
function codemetrics_drupal6_analyze_string($string, &$counts) {
  global $drupal6_internal_funcs;
  global $drupal6_internal_consts;
  global $lineno;

  $tokencount = array();
  $stats = array();
  $stats['lines'] = 0;
  $stats['comment-lines'] = 0;
  $stats['functions'] = 0;

  $lineno = 0;  
  $tokens = token_get_all($string);
  $tokens_seen = array();
  $classes = array();
  $consts = array();
  $vars = array();
  $funcdefs = array();
  $funccalls = array();

  while ($token = next($tokens)) {
    _codemetrics_inc_array($stats, 'tokens');
    if (is_string($token)) { // simple 1-character token
      codemetrics_drupal6_token_postprocess($token, $stats);
      _codemetrics_inc_array($tokens_seen, 'SIMPLE_TOKEN');
      $token_name = "SIMPLE_TOKEN";
    } 
    else { // token array
      list($id, $text) = $token;
      $token_name = token_name($id);
      _codemetrics_inc_array($tokens_seen, token_name($id));


      switch ($token_name) {
      case 'T_ML_COMMENT': // DRUPAL6 4
	_codemetrics_inc_array($tokencount, 'comment-multiline');
      case 'T_DOC_COMMENT': // PHP 5+
	_codemetrics_inc_array($tokencount, 'comment-docstyle');
	_codemetrics_inc_array($tokencount, 'comment-multiline');
      case 'T_COMMENT':
	_codemetrics_inc_array($tokencount, 'comment-inline');
	$num_comment_lines = _codemetrics_linecount($text);
	$stats['comment-lines'] += $num_comment_lines;
	codemetrics_drupal6_token_postprocess($token, $stats);
	break;
	
      case 'T_FUNCTION':
	codemetrics_drupal6_token_postprocess($token, $stats);
	_codemetrics_inc_array($stats, 'functions');
	_codemetrics_inc_array($tokencount, 'function');
	$token = codemetrics_drupal6_token_skip($tokens);
	$funcname = $token[1];
	$funcdefs[$funcname] = $lineno + 2;
	codemetrics_drupal6_token_postprocess($token, $stats);
	break;
	
      case 'T_VARIABLE':
	_codemetrics_inc_array($tokencount, 'variables');
	$vars[$text][] = $lineno + 2;
	$stats['variables-distinct'] = count($vars);
	codemetrics_drupal6_token_postprocess($token, $stats);
	break;

      case 'T_STRING':
	_codemetrics_inc_array($tokencount, 'literal-string');
	$tokenvalue = $token[1];
	$nexttoken = codemetrics_drupal6_token_peek($tokens);
	$nexttokenval = 
	  is_string($nexttoken) ?
	    $nexttokenval = $nexttoken : 
  	    $nexttokenval = $nexttoken[1];
	if (in_array($tokenvalue, $drupal6_internal_funcs)) {
	  if ($nexttokenval == '(') { // function call?
	    _codemetrics_inc_array($tokencount, 'drupal6-internal-func-call');
	    $funccalls['drupal6_internal'][$tokenvalue][] = $lineno + 2;
	  }
	  else {
	    _codemetrics_inc_array($tokencount, 'drupal6-internal-func-other');
	  }
	}
	elseif (array_key_exists($tokenvalue, $drupal6_internal_consts)) {
	  _codemetrics_inc_array($tokencount, 'php-internal-const-use');
	}
	else {
	  if ($nexttokenval == '(') { // function call?
	    _codemetrics_inc_array($tokencount, 'php-user-func-call');
	    $funccalls['user'][$tokenvalue][] = $lineno + 2;
	  }
	  else {
	    _codemetrics_inc_array($tokencount, 'php-other-literal-string');
	  }
	}
	break;
	
      case 'T_CONST':
	_codemetrics_inc_array($tokencount, 'constant');
	$token = codemetrics_drupal6_token_skip($tokens);
	$constname = $token[1];
	$consts[$constname] = $lineno + 2; // note line number
	codemetrics_drupal6_token_postprocess($token, $stats);
	break;

      case 'T_CLASS':
	_codemetrics_inc_array($tokencount, 'class');
	$token = codemetrics_drupal6_token_skip($tokens);
	$classname = $token[1];
	$classes[$classname] = $lineno + 2; // note line number
	$stats['class-distinct'] = count($classes);
	codemetrics_drupal6_token_postprocess($token, $stats);
	break;

      default:
	codemetrics_drupal6_token_process($token, $tokencount, $stats);
	break;
      }
    }
  }

  // compensate for last line  
  _codemetrics_inc_array($stats, 'lines');
  $stats['filesize'] = strlen($string);

  $counts['tokencount'] = $tokencount;
  $counts['vars'] = $vars;
  $counts['funcdefs'] = $funcdefs;
  $counts['funccalls'] = $funccalls;
  $counts['consts'] = $consts;
  $counts['classes'] = $classes;
  $counts['tokens'] = $tokens_seen;

  $stats['ctrl-complexity'] = codemetrics_drupal6_ctrl_complexity($counts);

  $stats['funccalls-all'] = codemetrics_drupal6_all_funccalls($counts);
  $stats['funccalls-drupal'] = codemetrics_drupal6_drupal_funccalls($counts);
  $stats['funccalls-drupal-module'] = codemetrics_drupal6_internal_funccalls($counts);

  $stats['funccalls-distinct'] = 
    count($funccalls['drupal6_internal']) + 
    count($funccalls['user']);
  $stats['funccalls-distinct-drupal'] = count($funccalls['drupal6_internal']);

  $counts['stats'] = $stats;

  return $counts;
}

/** 
 *
 */
function codemetrics_drupal6_all_funccalls($counts) {
  $count = 0;
  $funccalls = $counts['funccalls'];
  if (count($funccalls)) {
    foreach ($funccalls as $type => $calls) {
      foreach ($calls as $func => $lines) {
	$count += count($lines);
      }
    }
  }
  return $count;
}

/** 
 *
 */
function codemetrics_drupal6_drupal_funccalls($counts) {
  $count = 0;
  $funccalls = $counts['funccalls']['drupal6_internal'];
  if (count($funccalls)) {
    foreach ($funccalls as $func => $lines) {
      $count += count($lines);
    }
  }
  return $count;
}

/** 
 *
 */
function codemetrics_drupal6_internal_funccalls($counts) {
  $count = 0;
  $funccalls = $counts['funccalls']['drupal6_internal'];
  $funcdefs = $counts['funcdefs'];
  if (count($funccalls)) {
    foreach ($funccalls as $func => $lines) {
      if (in_array($func, array_keys($funcdefs))) {
	$count += count($lines);
      }
    }
  }
  return $count;
}


/** 
 *
 */
function codemetrics_drupal6_ctrl_complexity(&$counts) {
  $tokencount = $counts['tokens'];
  $complexity_keys = array(
			   'T_IF',
			   'T_ELSE',
			   'T_ELSEIF',
			   'T_WHILE',
			   'T_DO',
			   'T_FOREACH',
			   'T_FOR',
			   'T_EVAL',
			   'T_SWITCH',
			   'T_CASE',
			   'T_BREAK',
			   'T_CONTINUE',
			   'T_EXIT',
			   'T_RETURN',
			   'T_TRY',
			   'T_CATCH',
			   'T_THROW',
			   );
  $ctrl_complexity = 0;
  foreach ($complexity_keys as $key) {
    $ctrl_complexity += (isset($tokencount[$key]) ? 
			 $tokencount[$key] : 
			 0);
  }
  return $ctrl_complexity;
}
