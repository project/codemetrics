<?php
/**
 * @file
 * Common code for PHP reporting and analysis.
 */

global $tokenmap; 
$tokenmap = array();

$tokenmap['T_ML_COMMENT'] = array('comment-multiline', 'comment-lines');
$tokenmap['T_DOC_COMMENT'] = array('comment-docstyle', 'comment-lines');
$tokenmap['T_COMMENT'] = array('comment-inline', 'comment-lines');

$tokenmap['T_START_HEREDOC'] = array('heredoc-start');
$tokenmap['T_END_HEREDOC'] = array('heredoc-end');

$tokenmap['T_VARIABLE'] = array('variable');
$tokenmap['T_STRING_VARNAME'] = array('varname');

$tokenmap['T_CONST'] = array('constant');

$tokenmap['T_FUNCTION'] = array('function');
$tokenmap['T_OLD_FUNCTION'] = array('function-old');

$tokenmap['T_VAR'] = array('var-declaration');
$tokenmap['T_DECLARE'] = array('declare');
$tokenmap['T_ENDDECLARE'] = array('enddeclare');

$tokenmap['T_ARRAY'] = array('array-definition');
$tokenmap['T_LIST'] = array('list-assignment');

$tokenmap['T_LNUMBER'] = array('literal-lnumber');
$tokenmap['T_DNUMBER'] = array('literal-dnumber');
$tokenmap['T_STRING'] = array('literal-string');
$tokenmap['T_CHARACTER'] = array('literal-char');
$tokenmap['T_ENCAPSED_AND_WHITESPACE'] = array('literal-enc-and-whitespace');
$tokenmap['T_CONSTANT_ENCAPSED_STRING'] = array('literal-const-enc-string');

$tokenmap['T_NUM_STRING'] = array('type-num-string');

$tokenmap['T_ARRAY_CAST'] = array('cast-array');
$tokenmap['T_BOOL_CAST'] = array('cast-boolean');
$tokenmap['T_DOUBLE_CAST'] = array('cast-double');
$tokenmap['T_OBJECT_CAST'] = array('cast-object');
$tokenmap['T_STRING_CAST'] = array('cast-string');
$tokenmap['T_INT_CAST'] = array('cast-int');
$tokenmap['T_UNSET_CAST'] = array('cast-unset'); // not implemented

$tokenmap['T_PRIVATE'] = array('vis-private');
$tokenmap['T_PROTECTED'] = array('vis-protected');
$tokenmap['T_PUBLIC'] = array('vis-public');

$tokenmap['T_STATIC'] = array('scope-static');
$tokenmap['T_GLOBAL'] = array('scope-global');

$tokenmap['T_CLASS'] = array('obj-class');
$tokenmap['T_ABSTRACT'] = array('obj-abstract');
$tokenmap['T_INTERFACE'] = array('obj-interface');
$tokenmap['T_INSTANCEOF'] = array('obj-instanceof');
$tokenmap['T_EXTENDS'] = array('obj-extends');
$tokenmap['T_FINAL'] = array('obj-final');
$tokenmap['T_IMPLEMENTS'] = array('obj-implements');
$tokenmap['T_NEW'] = array('obj-new');
$tokenmap['T_CLONE'] = array('obj-clone');

$tokenmap['T_WHITESPACE'] = array('lex-whitespace');
$tokenmap['T_BAD_CHARACTER'] = array('lex-bad_character');
$tokenmap['T_CURLY_OPEN'] = array('lex-curly-open');
$tokenmap['T_DOLLAR_OPEN_CURLY_BRACES'] = array('lex-dollar-open-curly-braces');
$tokenmap['T_OPEN_TAG'] = array('lex-tag-open');
$tokenmap['T_OPEN_TAG_WITH_ECHO'] = array('lex-tag-open-with-echo');
$tokenmap['T_CLOSE_TAG'] = array('lex-tag-close');
$tokenmap['T_INLINE_HTML'] = array('lex-inline-html');

$tokenmap['T_IF'] = array('ctrl-if');
$tokenmap['T_ENDIF'] = array('ctrl-endif');
$tokenmap['T_ELSE'] = array('ctrl-else');
$tokenmap['T_ELSEIF'] = array('ctrl-elseif');
$tokenmap['T_DO'] = array('ctrl-do');
$tokenmap['T_EVAL'] = array('ctrl-eval');
$tokenmap['T_EXIT'] = array('ctrl-exit');
$tokenmap['T_RETURN'] = array('ctrl-return');
$tokenmap['T_CONTINUE'] = array('ctrl-continue');
$tokenmap['T_SWITCH'] = array('ctrl-switch');
$tokenmap['T_ENDSWITCH'] = array('ctrl-endswitch');
$tokenmap['T_CASE'] = array('ctrl-case');
$tokenmap['T_BREAK'] = array('ctrl-break');
$tokenmap['T_DEFAULT'] = array('ctrl-default');
$tokenmap['T_FOREACH'] = array('ctrl-foreach');
$tokenmap['T_ENDFOREACH'] = array('ctrl-endforeach');
$tokenmap['T_WHILE'] = array('ctrl-while');
$tokenmap['T_ENDWHILE'] = array('ctrl-endwhile');
$tokenmap['T_FOR'] = array('ctrl-for');
$tokenmap['T_ENDFOR'] = array('ctrl-endfor');
$tokenmap['T_TRY'] = array('ctrl-try');
$tokenmap['T_CATCH'] = array('ctrl-catch');
$tokenmap['T_THROW'] = array('ctrl-throw');
$tokenmap['T_AS'] = array('ctrl-each-as-clause');

$tokenmap['T_BOOLEAN_OR'] = array('boolean-or');
$tokenmap['T_BOOLEAN_AND'] = array('boolean-and');

$tokenmap['T_LOGICAL_OR'] = array('op-logical-or');
$tokenmap['T_LOGICAL_XOR'] = array('op-logical-xor');
$tokenmap['T_LOGICAL_AND'] = array('op-logical-and');
$tokenmap['T_INC'] = array('op-increment');
$tokenmap['T_DEC'] = array('op-decrement');

$tokenmap['T_AND_EQUAL'] = array('op-assign-and-equal');
$tokenmap['T_OR_EQUAL'] = array('op-assign-or-equal');
$tokenmap['T_CONCAT_EQUAL'] = array('op-assign-concat-equal');
$tokenmap['T_MOD_EQUAL'] = array('op-assign-mod-equal');
$tokenmap['T_DIV_EQUAL'] = array('op-assign-div-equal');
$tokenmap['T_MUL_EQUAL'] = array('op-assign-mul-equal');
$tokenmap['T_PLUS_EQUAL'] = array('op-assign-plus-equal');
$tokenmap['T_MINUS_EQUAL'] = array('op-assign-minus-equal');
$tokenmap['T_XOR_EQUAL'] = array('op-assign-xor-equal');
$tokenmap['T_SR_EQUAL'] = array('op-assign-shift-right_equal');
$tokenmap['T_SL_EQUAL'] = array('op-assign-shift-left-equal');
$tokenmap['T_UNSET'] = array('op-unset');
$tokenmap['T_SR'] = array('op-shift-right');
$tokenmap['T_SL'] = array('op-shift-left');
$tokenmap['T_DOUBLE_ARROW'] = array('op-array-elt-ref');
$tokenmap['T_OBJECT_OPERATOR'] = array('op-object-elt-ref');
$tokenmap['T_DOUBLE_COLON'] = array('op-scope-resolution');

$tokenmap['T_ECHO'] = array('output-echo');
$tokenmap['T_PRINT'] = array('output-print');

$tokenmap['T_ISSET'] = array('func-isset');
$tokenmap['T_EMPTY'] = array('func-empty');

$tokenmap['T_IS_EQUAL'] = array('comp-equal');
$tokenmap['T_IS_NOT_EQUAL'] = array('comp-not-equal');
$tokenmap['T_IS_SMALLER_OR_EQUAL'] = array('comp-smaller_or_equal');
$tokenmap['T_IS_GREATER_OR_EQUAL'] = array('comp-greater_or_equal');
$tokenmap['T_IS_IDENTICAL'] = array('comp-identical');
$tokenmap['T_IS_NOT_IDENTICAL'] = array('comp-not-identical');

$tokenmap['T_REQUIRE'] = array('file-require');
$tokenmap['T_REQUIRE_ONCE'] = array('file-require-once');
$tokenmap['T_INCLUDE'] = array('file-include');
$tokenmap['T_INCLUDE_ONCE'] = array('file-include-once');

$tokenmap['T_FILE'] = array('meta-file');
$tokenmap['T_LINE'] = array('meta-line');
$tokenmap['T_FUNC_C'] = array('meta-func');
$tokenmap['T_CLASS_C'] = array('meta-class');
$tokenmap['T_HALT_COMPILER'] = array('meta-halt-compiler');

$tokenmap['T_USE'] = array('use-stmt'); // not implemented


function codemetrics_php_supports_types() {
  return array('php' =>'php');
}

/* Map file extensions to types. */
function codemetrics_php_file_extensions() {
  return array(
	       'generic' => array(
				  'class' =>'php',
				  'engine' =>'php',
				  'inc' =>'php',
				  'install' =>'php',
				  'module' =>'php',
				  'php' =>'php',
				  'php4' =>'php',
				  'php5' =>'php',
				  'profile' =>'php',
				  'theme' =>'php',
				  ),
	       );	       
}
